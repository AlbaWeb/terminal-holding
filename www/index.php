<?php
$siteName='My Little Website';


``
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Holding Page</title>
<link href='http://fonts.googleapis.com/css?family=Ubuntu+Mono:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/dynatexer.dev.js"></script>
<link type="text/css" rel="stylesheet" href="css/style.css" />

<script type="text/javascript">
	jQuery(document).ready(function(){
		$('.content').dynatexer({
			loop: 1,
			content: [
				{
					animation: 'additive',
					delay: 80,
					placeholder: '<span class="console_text">',
					render_strategy: 'text-by-char',
					items: "> Welcome to <?php echo $siteName; ?>\n> As you can see it's still undergoing construction for the moment.\n\n> You can follow me on the following social networks until this site is ready.\n\n> Twitter: @lazysod\n> Google Plus: go.googleplus.me/barrysmith\n> Homepage: barrysmith.org\n> "
				},


				{
					delay: 2000
				},
			],
			cursor: {
				animation: 'replace',
				loop: 'infinite',
				delay: 500,
				placeholder: '<span class="console_cursor">',
				render_strategy: 'array-items',
				items: ['&nbsp;', '']
			}
		});
		$('.content').dynatexer('play', function() {

			});
		$('#play').click(function(e) {
			$('.content').dynatexer('play');
			e.preventDefault();
		});
		$('#pause').click(function(e) {
			$('.content').dynatexer('pause');
			e.preventDefault();
		});
		$('#reset').click(function(e) {
			$('.content').dynatexer('reset');
			e.preventDefault();
		});
		$('#configure').click(function(e) {
			$('.content').dynatexer('configure', {
				loop: 1,
				content: [
					{
						animation: 'additive',
						delay: 50,
						placeholder: '<span class="console_text">',
						render_strategy: 'text-by-char',
						items: "Simple, un texto corto y se acabó."
					}
				],
				cursor: {
					animation: 'replace',
					loop: 'infinite',
					delay: 500,
					placeholder: '<span class="console_cursor">',
					render_strategy: 'array-items',
					items: ['&nbsp;', '']
				}
			});
			e.preventDefault();
		});
	});
</script>
</head>

<body>

	<h1 align="center">Welcome to <?php echo $_SERVER['HTTP_HOST']; ?></h1>
	<?php
		echo '<h2 align="center">Your IP address is <font color="#ff7c7c">'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'</font></h2>';
	?>

	<div class="console_title centerThis">
		<span>Terminal</span>
	</div>
	<div class="console centerThis">
		<div class="content">
		</div>
	</div>

</body>
</html>
